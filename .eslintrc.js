module.exports = {
    "extends": ["plugin:vue/essential"],
    "root": true,
    "parserOptions": {
        "ecmaVersion": 2018,
        "parser": "babel-eslint",
        "sourceType": "module"
    },

    "env": {
        "node": true
    },

    "globals": {
        "document": false,
        "navigator": false,
        "window": false
    },

    "rules": {
        "indent": ["error", 4],
        "quotes": [
            "error",
            "single",
            {
                "allowTemplateLiterals": true
            }
        ],
        "arrow-parens": ["error", "always"],
        "semi": ["error", "always"]
    }
}