import { API_PREFIX } from '~/consts';
import path from 'path';
import ajax from '~/modules/ajax';

export const init = async () => {
    try {
        const res = await { data: { wow: 'nice!' } };
        return res.data;
    } catch (err) {
        return err;
    }
};
