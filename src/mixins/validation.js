import { validationMixin } from 'vuelidate';
import isValid from '~/directives/is-valid';
import { getValueByStringKey } from '~/tools';
import AppValidationError from '~/components/AppValidationError.vue';

export default {
    components: {
        AppValidationError
    },
    mixins: [validationMixin],
    directives: {
        isValid
    },
    methods: {
        validationError(stringKey, message) {
            const $v = getValueByStringKey(this.$v, stringKey);

            if ($v.$dirty) {
                const props = Object.keys($v).filter((prop) => !prop.startsWith('$'));
                for (let index in props) {
                    const prop = props[index];

                    if (!$v[prop]) {
                        if (typeof message === 'object') {
                            if (message[prop]) {
                                return message[prop];
                            } else {
                                return message.default;
                            }
                        } else {
                            return message;
                        }
                    }
                }
            }
        }
    }
};
