import { init } from '~/api/app';

export const state = () => ({
    initialized: false
});

export const getters = {
    initialized: (state) => state.initialized
};

export const mutations = {
    SET_INITIALIZED(state) {
        state.initialized = true;
    }
};

export const actions = {
    init: async ({ commit }) => {
        const data = await init();
        commit('SET_INITIALIZED');
        return true;
    }
};
