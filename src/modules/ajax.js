import axios from 'axios';

const ajax = axios.create({
    baseURL: process.env.API_URL,
    withCredentials: true,
    timeout: 10000,
    xsrfCookieName: 'xcsrftoken',
    xsrfHeaderName: 'x-csrftoken'
});

export default ajax;
