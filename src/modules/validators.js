import { sameAs } from 'vuelidate/lib/validators';
export * from 'vuelidate/lib/validators';

export const checked = sameAs(() => true);
