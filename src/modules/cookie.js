import cookier from 'cookie';

export const get = (prop, req) => {
    let cookies = process.browser ? document.cookie : req ? req.headers['cookie'] : '';
    cookies = cookier.parse(cookies || '');

    return cookies[prop];
};

export const set = (prop, res) => {
    if (process.browser) {
        const result = [];
        const cookies = cookier.parse(document.cookie);
        cookies[prop] = value;
        for (let key in cookies) {
            result.push(`${key}=${cookies[key]}`);
        }
        document.cookie = result.join('; ');
    } else if (res) {
        res.setHeader('Set-Cookie', [`${prop}=${value}`]);
    }
};
