import ResizeObserver from 'resize-observer-polyfill';

export default {
    inserted($el, { value }) {
        const $target = value.target;
        const direction = value.direction || 'top';
        const corner = value.corner;

        $el.style.position = 'fixed';
        $el.style.visibility = 'hidden';

        const attach = (dir = direction, responsive = true) => {
            var windowWidth = window.innerWidth;
            var windowHeight = window.innerHeight;
            const elRect = $el.getBoundingClientRect();
            const targetRect = $target.getBoundingClientRect();

            let pos = { left: 0, top: 0 };

            if (dir === 'left' || dir === 'right') {
                pos.top = targetRect.top + targetRect.height / 2 - elRect.height / 2;

                if (dir === 'left') {
                    pos.left = targetRect.left - elRect.width;
                } else if (dir === 'right') {
                    pos.left = targetRect.left + targetRect.width;
                }
            }

            if (dir === 'top' || dir === 'bottom') {
                if (corner === 'right') {
                    pos.left = targetRect.left + targetRect.width - elRect.width;
                } else if (corner === 'left') {
                    pos.left = targetRect.left;
                } else {
                    pos.left = targetRect.left + targetRect.width / 2 - elRect.width / 2;
                }

                if (dir === 'top') {
                    pos.top = targetRect.top - elRect.height;
                } else if (dir === 'bottom') {
                    pos.top = targetRect.top + targetRect.height;
                }
            }

            $el.style.left = `${pos.left}px`;
            $el.style.top = `${pos.top}px`;

            setTimeout(() => {
                $el.style.visibility = '';
            });

            if (responsive) {
                if (dir === 'left') {
                    if (pos.left <= 0) {
                        attach('right', false);
                    }
                } else if (dir === 'right') {
                    if (pos.left + elRect.width >= windowWidth) {
                        attach('left', false);
                    }
                } else if (dir === 'top') {
                    if (pos.top <= 0) {
                        attach('bottom', false);
                    }
                } else if (dir === 'bottom') {
                    if (pos.top + elRect.height >= windowHeight) {
                        attach('top', false);
                    }
                }
            }
        };

        const ro = new ResizeObserver(() => {
            attach();
        });

        $el.__attach_fn__ = () => attach();
        $el.__attach_ro__ = ro;

        ro.observe($el);
        ro.observe($target);

        document.addEventListener('scroll', $el.__attach_fn__, true);
        document.addEventListener('touchmove', $el.__attach_fn__);
        window.addEventListener('resize', $el.__attach_fn__);

        attach();
    },
    unbind($el) {
        document.removeEventListener('scroll', $el.__attach_fn__);
        document.removeEventListener('touchmove', $el.__attach_fn__);
        window.removeEventListener('resize', $el.__attach_fn__);
        $el.__attach_ro__.disconnect();
    }
};
