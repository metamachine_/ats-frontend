export default async ({ store, req, res, redirect }) => {
    if (!store.getters.initialized) {
        try {
            await store.dispatch('init');
        } catch (err) {
            console.log(err);
            return redirect('/404');
        }
    }
};
