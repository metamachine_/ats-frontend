export const getValueByStringKey = (obj, stringKey) => {
    stringKey = stringKey.replace(/\[(\w+)\]/g, '.$1');
    stringKey = stringKey.replace(/^\./, '');
    const a = stringKey.split('.');

    for (var i = 0, n = a.length; i < n; ++i) {
        let k = a[i];
        if (k in obj) {
            obj = obj[k];
        } else {
            return;
        }
    }
    return obj;
};

export const getExcludedObjectByProps = (obj, props = []) => {
    obj = { ...obj };

    props.map((prop) => {
        if (obj[prop]) {
            delete obj[prop];
        }
    });

    return obj;
};

export const findParentByElement = ($el, $parentEl, self = true) => {
    if (!$el) {
        return null;
    }

    if (self && $el === $parentEl) {
        return $el;
    }

    while (($el = $el.parentElement) && $el !== $parentEl);
    return $el;
};
