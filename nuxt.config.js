const dotenv = require('dotenv-flow');

const isDevelopment = process.env.NODE_ENV === 'development' ? true : false;

dotenv.config({
    node_env: process.env.NODE_ENV || 'production'
});

module.exports = {
    srcDir: 'src/',
    env: process.env,

    modules: [['@nuxtjs/style-resources']],
    vendor: ['axios', 'nouislider', 'resize-observer-polyfill', 'v-click-outside'],
    plugins: [],
    router: {
        middleware: ['init']
    },

    head: {
        title: 'Metamachine',
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: 'Metachine description' }
        ],
        link: [{ rel: 'icon', type: 'image/x-icon', href: '/images/favicon.ico' }]
    },

    loading: false,
    css: [
        {
            src: '~/stylesheets/index.styl',
            lang: 'stylus'
        }
    ],

    build: {
        postcss: {
            plugins: {
                'postcss-discard-comments': {}
            },
            presets: {
                autoprefixer: {}
            }
        },
        extractCSS: !isDevelopment,
        optimization: {
            minimize: false,
            splitChunks: {
                cacheGroups: {
                    styles: {
                        name: 'styles',
                        test: /\.(css|vue)$/,
                        chunks: 'all',
                        enforce: true
                    }
                }
            }
        }
    },

    styleResources: {
        stylus: ['~/stylesheets/vars.styl', '~/stylesheets/mixins.styl']
    }
};
